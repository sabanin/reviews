package com.reviews.models.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.reviews.models.Review;


@Repository
public interface ReviewRepo extends JpaRepository<Review, Long>{
	@Query("SELECT r FROM Review r WHERE r.appid = :appid")
	public List<Review> findFeatures(@Param("appid") String appid);
	
	@Query("SELECT r FROM Review r WHERE r.mainfeature = :feature")
	public List<Review> findReviewsWithFeature(@Param("feature") String feature);
	
	@Query("SELECT r FROM Review r WHERE r.mainfeature = :feature and r.appid = :appid and r.positive>=:positive")
	public List<Review> findPositiveReviewsByAppAndFeature(@Param("feature") String feature, 
			@Param("appid") String appid, 
			@Param("positive") Integer positive);
	
	@Query("SELECT r FROM Review r WHERE r.mainfeature = :feature and r.appid = :appid and r.negative<=-:negative")
	public List<Review> findNegativeReviewsByAppAndFeature(@Param("feature") String feature, 
			@Param("appid") String appid, 
			@Param("negative") Integer negative);

	@Query("SELECT COUNT(r) FROM Review r WHERE r.mainfeature = :feature and r.appid = :appid")
	public Long countReviewsByFeatureAndApp(@Param("feature") String feature, 
			@Param("appid") String appid);
	
	@Query("SELECT COUNT(r) FROM Review r WHERE r.mainfeature = :feature and r.appid = :appid and r.frequency>:frequency")
	public Long countReviewsByFeatureAppAndFrequency(@Param("feature") String feature, 
			@Param("appid") String appid,
			@Param("frequency") Integer frequency);
	
	@Query("SELECT COUNT(r) FROM Review r WHERE r.mainfeature = :feature and r.appid = :appid and r.positive>=:positive and r.positive>-r.negative")
	public Long countPositiveReviewsByFeatureAppAndLevel(@Param("feature") String feature, 
			@Param("appid") String appid,
			@Param("positive") Integer positive);
	
	@Query("SELECT COUNT(r) FROM Review r WHERE r.mainfeature = :feature and r.appid = :appid and r.negative<=-:negative and r.positive<=-r.negative")
	public Long countNegativeReviewsByFeatureAppAndLevel(@Param("feature") String feature, 
			@Param("appid") String appid,
			@Param("negative") Integer negative);
	
}
