package com.reviews.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Entity
@Data
@XmlRootElement
@Table(name = "newreviews2")
public class Review {
	
	@Id
    @GeneratedValue
	Long id;

	String comment; 
	String feature;
	String mainfeature;
	Integer frequency;
	String appid;
	Integer positive;
	Integer negative;
}
