package com.reviews.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.reviews.models.App;
import com.reviews.models.Review;
import com.reviews.models.repo.AppsRepo;
import com.reviews.models.repo.ReviewRepo;

@Controller
@RequestMapping("/reviews")
public class ViewController {

	@Autowired
	ReviewRepo repo;
	@Autowired
	AppsRepo appsRepo;
	
	@RequestMapping(value="/test", method = RequestMethod.GET)
	public String test(Model model){
		Review review = repo.findAll().get(0);
		model.addAttribute("review", review);
		return "reviews/test";
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public String chooseApp(Model model){
		List<Review> reviews = repo.findAll();
		Set<String> appids = new HashSet<>();
		for (Review review: reviews){
			appids.add(review.getAppid());
		}
		model.addAttribute("apps", appids);
		model.addAttribute("appsMap", getAllApps());
		return "reviews/apps";
	}
	
	@RequestMapping(value="/features/{appid}", method = RequestMethod.GET)
	public String getFeatures(@PathVariable String appid, Model model){
		List<Review> reviews = repo.findFeatures(appid);
		Map<String, Long> features = new HashMap();
		Map<String, HashSet<String>> synonyms = new HashMap();
		Integer maxSupport = 0;
		for (Review review: reviews){
			if (!(review.getMainfeature().contains(".") || review.getFeature().contains(".") ||
				  review.getMainfeature().contains("+") || review.getFeature().contains("+") ||
				  review.getMainfeature().contains("-") || review.getFeature().contains("-") ||
				  review.getMainfeature().contains("=") || review.getFeature().contains("=") ||
				  review.getMainfeature().contains("%") || review.getFeature().contains("%"))){
				String mainFeature = review.getMainfeature().replace("'", "");
				Integer frequency = review.getFrequency();
				maxSupport = frequency > maxSupport ? frequency : maxSupport;
				String feature = review.getFeature().replace("'", "");;
				features.put(mainFeature, (long) (int)frequency);
				if (synonyms.containsKey(mainFeature)){
					if (!feature.equals(mainFeature)) synonyms.get(mainFeature).add(feature);
				} else {
					if (!feature.equals(mainFeature)){
						synonyms.put(mainFeature, new HashSet<String>());
						synonyms.get(mainFeature).add(feature);
					}
				}
			}
		}
		List<String> featuresList = sortHashMapByValues(features);
		model.addAttribute("featuresList", featuresList);
		model.addAttribute("featuresFreqency", features);
		model.addAttribute("synonyms", synonyms);
		model.addAttribute("appid", appid);
		model.addAttribute("appsMap", getAllApps());
		model.addAttribute("maxsupport", maxSupport);
		return "reviews/features";
	}
	
	@RequestMapping(value="/getapps", method = RequestMethod.POST)
	public String getApps(HttpServletRequest request, Model model){
		String[] features = request.getParameterValues("feature");
		HashMap<String, Long> apps = new HashMap();
		if (features == null){
			return getFeatures(request.getParameter("appid"), model);
		}
		for (String feature : features){
			List<Review> reviews = repo.findReviewsWithFeature(feature);
			Set<String> applicationsWithFeature = new HashSet<String>();
			for (Review review : reviews){
				applicationsWithFeature.add(review.getAppid());
			}
			for (String app : applicationsWithFeature){
				if (apps.containsKey(app)){
					Long frequency = apps.get(app);
					frequency++;
					apps.put(app, frequency);
				} else {
					apps.put(app, 1L);
				}
			}
		}
		List<String> sortedApps = sortHashMapByValues(apps);
		model.addAttribute("appid", request.getParameter("appid"));
		model.addAttribute("featureamount", features.length);
		model.addAttribute("features", features);
		model.addAttribute("apps", sortedApps);
		model.addAttribute("appsFrequency", apps);
		model.addAttribute("appsMap", getAllApps());
		return "reviews/chooseapps";
	}
	
	@RequestMapping(value="/getstatistic", method = RequestMethod.POST)
	public String getStatistics(HttpServletRequest request, Model model){
		String[] features = request.getParameterValues("feature");
		String[] apps = request.getParameterValues("apps");	
		String baseApp = request.getParameter("appid");
		
		if (apps == null){
			return getApps(request, model);
		}
		
		Long maxSupport = 0L;
		for (String app : apps){
			for (String feature : features){
				Long count = repo.countReviewsByFeatureAndApp(feature, app);
				maxSupport = count>maxSupport ? count : maxSupport;
			}
		}
		model.addAttribute("maxsupport", maxSupport);
		model.addAttribute("features", request.getParameterValues("feature"));
		model.addAttribute("apps", request.getParameterValues("apps"));
		model.addAttribute("appid", baseApp);
		model.addAttribute("appsMap", getAllApps());
		return "reviews/statistics";
	}
	
	@RequestMapping(value="/gettable", method = RequestMethod.POST)
	public String getTable(HttpServletRequest request, Model model){
		String[] features = request.getParameterValues("feature");
		String[] apps = request.getParameterValues("apps");	
		String baseApp = request.getParameter("appid");
		
		Long maxSupport = 0L;
		for (String app : apps){
			for (String feature : features){
				Long count = repo.countReviewsByFeatureAndApp(feature, app);
				maxSupport = count>maxSupport ? count : maxSupport;
			}
		}
		model.addAttribute("maxsupport", maxSupport);
		model.addAttribute("features", request.getParameterValues("feature"));
		model.addAttribute("apps", request.getParameterValues("apps"));
		model.addAttribute("appid", baseApp);
		model.addAttribute("appsMap", getAllApps());
		return "reviews/table";
	}
	
	@RequestMapping(value="/getgraph", method = RequestMethod.POST)
	public String getGraph(HttpServletRequest request, Model model){
		String[] features = request.getParameterValues("feature");
		String[] apps = request.getParameterValues("apps");	
		String baseApp = request.getParameter("appid");
		
		Long maxSupport = 0L;
		for (String app : apps){
			for (String feature : features){
				Long count = repo.countReviewsByFeatureAndApp(feature, app);
				maxSupport = count>maxSupport ? count : maxSupport;
			}
		}
		model.addAttribute("maxsupport", maxSupport);
		model.addAttribute("features", request.getParameterValues("feature"));
		model.addAttribute("apps", request.getParameterValues("apps"));
		model.addAttribute("appid", baseApp);
		model.addAttribute("appsMap", getAllApps());
		return "reviews/graph";
	}
	
//	@RequestMapping(value="/compare/{id}", method = RequestMethod.GET)
//	public String compareTable(@PathVariable String id, Model model){
//		List<Review> reviews = repo.findFeatures(id);
//		Map<String, Long> features = new HashMap();
//		Map<String, Long> apps = new HashMap();
//		for (Review review : reviews){
//			String feature = review.getFeature();
//			features.put(feature, 1L);
//		}
//		
//		for (String feature: features.keySet()){
//			List<Review> reviewsWithFeature = repo.findReviewsWithFeature(feature);
//			Set<String> appsWithFeature = new HashSet<String>();
//			for (Review reviewWithFeature : reviewsWithFeature){
//				appsWithFeature.add(reviewWithFeature.getAppid());
//			}
//			features.put(feature, new Long(appsWithFeature.size()));			
//			for (String app : appsWithFeature){
//				if(apps.containsKey(app)){
//					Long frequency = apps.get(app);
//					frequency++;
//					apps.put(app, frequency);
//				} else {						
//					apps.put(app, 1L);
//				}			
//			}
//		}
//		List<String> featuresList = sortHashMapByValues(features);
//		model.addAttribute("appsfrequency", apps);
//		model.addAttribute("apps", sortHashMapByValues(apps));
//		model.addAttribute("features", featuresList);
//		Integer i=0;
//		for (String feature: featuresList){
//			for (String app : apps.keySet()){
//				model.addAttribute("p_"+i+"_"+app, repo.findPositiveReviewsByAppAndFeature(feature, app, SENTIMENT_LEVEL).size());
//				model.addAttribute("n_"+i+"_"+app, repo.findNegativeReviewsByAppAndFeature(feature, app, -SENTIMENT_LEVEL).size());	
//			}
//			i++;
//		}
//		model.addAttribute("appsMap", getAllApps());
//		return "reviews/compare";
//	}
	
	@RequestMapping("/get")
	public String getReviews(@PathParam("appid") String appid, 
							 @PathParam("feature") String feature, 
							 @PathParam("level") String level, Model model){
		List<Review> comments = repo.findPositiveReviewsByAppAndFeature(feature, appid, 0);
		List<Review> goodComments = new ArrayList<Review>();
		List<Review> badComments = new ArrayList<Review>();
		HashSet<String> synonyms = new HashSet();
		Integer sentimentLevel = new Integer(level);
		for (Review review : comments){
			if (!review.getMainfeature().equals(review.getFeature())){
				synonyms.add(review.getFeature());
			}
			if (review.getPositive()>=sentimentLevel || review.getNegative()<=-sentimentLevel){
				Integer score = review.getPositive()+review.getNegative();
				if (score>0){
					review.setComment(review.getComment()+" [+"+review.getPositive()+"]["+review.getNegative()+"]");
					goodComments.add(review);
				} else {
					review.setComment(review.getComment()+" [+"+review.getPositive()+"]["+review.getNegative()+"]");
					badComments.add(review);
				}
			}
		}
		model.addAttribute("goodcomments", goodComments);
		model.addAttribute("badcomments", badComments);
		model.addAttribute("app", appid);
		model.addAttribute("synonyms", synonyms);
		model.addAttribute("feature", feature);
		model.addAttribute("appsMap", getAllApps());
		return "reviews/comments";
	}
	
	public List<String> sortHashMapByValues(
	        Map<String, Long> passedMap) {
	    List<String> mapKeys = new ArrayList<>(passedMap.keySet());
	    List<Long> mapValues = new ArrayList<>(passedMap.values());
	    Collections.sort(mapValues, Collections.reverseOrder());
	    Collections.sort(mapKeys, Collections.reverseOrder());

	    List<String> sortedMap =
	        new ArrayList<>();

	    Iterator<Long> valueIt = mapValues.iterator();
	    while (valueIt.hasNext()) {
	        Long val = valueIt.next();
	        Iterator<String> keyIt = mapKeys.iterator();

	        while (keyIt.hasNext()) {
	            String key = keyIt.next();
	            Long comp1 = passedMap.get(key);
	            Long comp2 = val;

	            if (comp1.equals(comp2)) {
	                keyIt.remove();
	                sortedMap.add(key);
	                break;
	            }
	        }
	    }
	    return sortedMap;
	}
	
	private String cleanCharacters(String input){
		return input.replace("%", "percent").replace("+", "plus");
	}
	
	private Map<String, App> getAllApps(){
		List<App> apps = appsRepo.findAll();
		Map<String, App> result = new HashMap();
		for (App app : apps){
			result.put(app.getAppid(), app);
		}
		return result;
	}
}

