package com.reviews.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;


@Entity
@Data
@XmlRootElement
@Table(name = "apps")
public class App {
		
		@Id
	    @GeneratedValue
		Long id;

		String img;
		String name;
		String appid;
}
