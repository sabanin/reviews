package com.reviews.controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;

import org.hibernate.hql.internal.ast.tree.MapEntryNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.reviews.Data;
import com.reviews.Data.DataPoint;
import com.reviews.models.App;
import com.reviews.models.Review;
import com.reviews.models.repo.AppsRepo;
import com.reviews.models.repo.ReviewRepo;

@RestController
@RequestMapping("/reviews/rest")
public class RESTController {
	@Autowired
	ReviewRepo repo;
	@Autowired 
	AppsRepo appsRepo;
	
	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody List<Review> findReview(){
		return repo.findAll();
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public void addReview(@RequestParam String appid,
						  @RequestParam String comment,
						  @RequestParam String feature,
						  @RequestParam String mainfeature,
						  @RequestParam Integer frequency,
						  @RequestParam Integer negative,
						  @RequestParam Integer positive){
		Review review = new Review();
		review.setAppid(appid);
		review.setComment(comment);
		review.setFeature(feature);
		review.setMainfeature(mainfeature);
		review.setFrequency(frequency);
		review.setNegative(negative);
		review.setPositive(positive);
		repo.saveAndFlush(review);
	}
	
//	@RequestMapping(value="/deleteapp", method=RequestMethod.DELETE)
//	public void delete(@RequestParam String appid){
//		List<Review> reviews = repo.findFeatures(appid);
//		repo.delete(reviews);
//	}
	
	@RequestMapping(value="/app", method = RequestMethod.POST)
	public void addReview(@RequestParam String appid,
						  @RequestParam String name,
						  @RequestParam String img){
		App app = new App();
		app.setAppid(appid);
		app.setImg(img);
		app.setName(name);
		appsRepo.saveAndFlush(app);
	}
	
	@RequestMapping(value="/compare/{id}", method = RequestMethod.GET)
	public @ResponseBody Set<String> compareTable(@PathVariable String id, Model model){
		List<Review> reviews = repo.findFeatures(id);
		Map<String, Long> features = new HashMap();
		Map<String, Long> apps = new HashMap();
		for (Review review : reviews){
			String feature = review.getFeature();
			if (features.containsKey(feature)){
				Long frequency = features.get(feature);
				frequency++;
				features.put(feature, frequency);
			} else {
				features.put(feature, 1L);
			}
		}
		
		for (String feature: features.keySet()){
			List<Review> reviewsWithFeature = repo.findReviewsWithFeature(feature);
			Set<String> appsWithFeature = new HashSet<String>();
			for (Review reviewWithFeature : reviewsWithFeature){
				appsWithFeature.add(reviewWithFeature.getAppid());
			}
			for (String app : appsWithFeature){
				if(apps.containsKey(app)){
					Long frequency = apps.get(app);
					frequency++;
					apps.put(app, frequency);
				} else {						
					apps.put(app, 1L);
				}			
			}
		}
		List<String> featuresList = sortHashMapByValues(features);
		model.addAttribute("apps", sortHashMapByValues(apps));
		model.addAttribute("features", featuresList);
		Integer i=0;
		for (String feature: featuresList){
			for (String app : apps.keySet()){
				model.addAttribute("p_"+i+"_"+app, repo.findPositiveReviewsByAppAndFeature(feature, app, 2).size());
				model.addAttribute("n_"+i+"_"+app, repo.findNegativeReviewsByAppAndFeature(feature, app, -2).size());	
			}
			i++;
		}
		return model.asMap().keySet();
	}
	
	@RequestMapping(value="/piechart", method = RequestMethod.POST)
	public PieChartData getPieData(HttpServletRequest request){
		String feature = request.getParameter("feature");
		String app = request.getParameter("app");
		Integer levelSentiment = new Integer(request.getParameter("levelsentiment"));
		Long levelSupport = new Long(request.getParameter("levelsupport"));
		
		List<Data> result = new ArrayList<Data>();
		PieChartData pcd = new PieChartData();
		Data data = new Data();
		pcd.setDivid(app + "_" + feature);
		data.setToolTipContent("<a target='_blank' href = {name}>Show reviews</a><hr/>{y}");
		DataPoint dataPointPositive = data.new DataPoint();
		DataPoint dataPointNegative = data.new DataPoint();
		dataPointPositive.setLabel("positive");
		dataPointNegative.setLabel("negative");
		Long positiveReviews = repo.countPositiveReviewsByFeatureAppAndLevel(feature, app, levelSentiment);
		Long negativeReviews = repo.countNegativeReviewsByFeatureAppAndLevel(feature, app, levelSentiment);
		if (positiveReviews+negativeReviews < levelSupport){
			pcd.setShow(false);
		}
		dataPointPositive.setY(positiveReviews.doubleValue());
		dataPointNegative.setY(negativeReviews.doubleValue());
		
		//add to lists
		List<DataPoint> dataPoints = new ArrayList<Data.DataPoint>();
		dataPointNegative.setName("/reviews/get?appid="+app+"&feature="+feature+"&level="+levelSentiment.toString());
		dataPointPositive.setName("/reviews/get?appid="+app+"&feature="+feature+"&level="+levelSentiment.toString());
		dataPoints.add(dataPointPositive);
		dataPoints.add(dataPointNegative);
		data.setName("Positive/Negative");
		data.setType("pie");
		data.setDataPoints(dataPoints);
		data.setShowInLegend(false);
		result.add(data);
		pcd.setData(result);
		return pcd;
		}
	
	@RequestMapping(value="/sentiment", method = RequestMethod.POST)
	public List<Data> getChartData(HttpServletRequest request){
		String[] features = request.getParameterValues("feature[]");
		String[] apps = request.getParameterValues("apps[]");
		String baseApp = request.getParameter("baseapp");
		Integer levelSentiment = new Integer(request.getParameter("levelsentiment"));
		Long levelSupport = new Long(request.getParameter("levelsupport"));
		Map<String, App> appsMap = getAllApps();
		
		String type = request.getParameter("type");
		List<Data> result = new ArrayList<Data>();
		for (String app : apps){
			Data application = new Data();
			application.setShowInLegend(true);
			if (app.equals(baseApp)){
				application.setName(appsMap.get(app).getName()+"(Base app)");
			}
			else{
				application.setName(appsMap.get(app).getName());
			}
			application.setToolTipContent("<a target='_blank' href = {name}>Show reviews</a><hr/>{y}");
			List<DataPoint> dataPoints = new ArrayList<Data.DataPoint>();
			for (String feature : features){
				DataPoint dataPoint = application.new DataPoint();
				dataPoint.setLabel(feature);
				dataPoint.setName("/reviews/get?appid="+app+"&feature="+feature+"&level="+levelSentiment.toString());
				if (type.equals("positive")){
					dataPoint.setY(getSentimentScoreSpecific(app, feature, levelSentiment, levelSupport, true));
				} else if (type.equals("negative")){
					dataPoint.setY(getSentimentScoreSpecific(app, feature, levelSentiment, levelSupport, false));
				} else {
					dataPoint.setY(getSentimentScore(app, feature, levelSentiment, levelSupport));
				}
				dataPoints.add(dataPoint);
			}
			application.setDataPoints(dataPoints);
			result.add(application);
		}
		return result;
	}
	
	@RequestMapping(value="/graph", method = RequestMethod.POST)
	public Map<String, ArrayList<String>> getGraph(HttpServletRequest request){
		String[] features = request.getParameterValues("features[]");
		String[] apps = request.getParameterValues("apps[]");
		String baseApp = request.getParameter("baseapp");
		Integer levelSentiment = new Integer(request.getParameter("levelsentiment"));
		Integer levelSupport = new Integer(request.getParameter("levelsupport"));
		Map<String, ArrayList<String>> result = new HashMap<String, ArrayList<String>>();
		for (String app:apps){
			result.put(app, new ArrayList<String>());
		}
		
		for (String app:apps){
			for (String feature:features){
				if (repo.countReviewsByFeatureAndApp(feature, app) >= levelSupport){
					result.get(app).add(feature);
				}
			}
		}
	
		return result;
	}
	
	public List<String> sortHashMapByValues(
	        Map<String, Long> passedMap) {
	    List<String> mapKeys = new ArrayList<>(passedMap.keySet());
	    List<Long> mapValues = new ArrayList<>(passedMap.values());
	    Collections.sort(mapValues, Collections.reverseOrder());
	    Collections.sort(mapKeys, Collections.reverseOrder());

	    List<String> sortedMap =
	        new ArrayList<>();

	    Iterator<Long> valueIt = mapValues.iterator();
	    while (valueIt.hasNext()) {
	        Long val = valueIt.next();
	        Iterator<String> keyIt = mapKeys.iterator();

	        while (keyIt.hasNext()) {
	            String key = keyIt.next();
	            Long comp1 = passedMap.get(key);
	            Long comp2 = val;

	            if (comp1.equals(comp2)) {
	                keyIt.remove();
	                sortedMap.add(key+" with "+val.toString()+" features");
	                break;
	            }
	        }
	    }
	    
	    return sortedMap;
	}
	
	private Double getSentimentScore(String appid, String feature, Integer level, Long levelsupport){
		List<Review> reviewsNegative = repo.findNegativeReviewsByAppAndFeature(feature, appid, level);
		List<Review> reviewsPositive = repo.findPositiveReviewsByAppAndFeature(feature, appid, level);
		Double sentimentScore = 0.0;
		for (Review review : reviewsNegative){
			sentimentScore+=review.getNegative();
		}
		for (Review review : reviewsPositive){
			sentimentScore+=review.getPositive();
		}
		if (reviewsNegative.size()+reviewsPositive.size()!=0){
			sentimentScore/=(reviewsNegative.size()+reviewsPositive.size());
		} else {
			return 0.0;
		}
		return (reviewsNegative.size()+reviewsPositive.size() >= levelsupport) ? sentimentScore : 0.0;
	}
	
	private Double getSentimentScoreSpecific(String appid, String feature, Integer level, Long levelsupport, boolean isPositive){
		List<Review> reviewsNegative = repo.findNegativeReviewsByAppAndFeature(feature, appid, level);
		List<Review> reviewsPositive = repo.findPositiveReviewsByAppAndFeature(feature, appid, level);
		
		Double sentimentScore = 0.0;
		for (Review review : isPositive ? reviewsPositive : reviewsNegative){
			sentimentScore+= isPositive ? review.getPositive() : review.getNegative();
		}
		if (reviewsNegative.size()+reviewsPositive.size()!=0){
			sentimentScore/=isPositive ? reviewsPositive.size() : reviewsNegative.size();
		} else {
			return 0.0;
		}
		return (reviewsNegative.size()+reviewsPositive.size() >= levelsupport) ? sentimentScore : 0.0;
	}
	
	@lombok.Data
	class PieChartData{
		List<Data> data;
		String divid;
		Boolean show = true;
	}
	
	private Map<String, App> getAllApps(){
		List<App> apps = appsRepo.findAll();
		Map<String, App> result = new HashMap();
		for (App app : apps){
			result.put(app.getAppid(), app);
		}
		return result;
	}
}
