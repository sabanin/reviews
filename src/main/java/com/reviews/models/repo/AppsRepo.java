package com.reviews.models.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.reviews.models.App;

@Repository
public interface AppsRepo extends JpaRepository<App, Long>{

	@Query("SELECT r FROM App r WHERE r.appid = :appid")
	public List<App> findAppByApid(@Param("appid") String appid);
	
}
