package com.reviews;

import java.util.List;

@lombok.Data
public class Data {
	String type = "column";
	String name;
	String toolTipContent;
	Boolean showInLegend = true;
	List<DataPoint> dataPoints;
	
	@lombok.Data
	public class DataPoint{
		String name;
		String label;
		Double y;
	}
}
